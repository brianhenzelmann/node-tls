const https = require("https"),
  fs = require("fs"),
  express = require('express');

const options = {
  key: fs.readFileSync("/tmp/key.pem"),
  cert: fs.readFileSync("/tmp/chain.pem")
};

const app = express();

let counter = 0;
app.use((req, res) => {
  console.log(`New request received ${++counter}`);
  res.sendFile(`${__dirname}/documents/cooper.jpeg`, {} , (err) => {
    if (err) {
      res.status(404).json({
        message: err.message,
      });
    }
  });
});

app.listen(8000);

https.createServer(options, app).listen(8080);
