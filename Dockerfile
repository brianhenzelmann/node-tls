FROM node:10-slim

WORKDIR /app

COPY ./ ./
RUN npm install --silent

CMD ["npm", "start"]
EXPOSE 8080
